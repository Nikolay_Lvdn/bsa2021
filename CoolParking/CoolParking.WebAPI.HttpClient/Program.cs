﻿using CoolParking.UI;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.HttpClientUI
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine("Greetings and salutations!\n" +
                "This is my bsa2021 task 3\n" +
                "You are welcome :)\n");

            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Enter command(# - to call /help):");
                Console.ForegroundColor = ConsoleColor.White;
                string input = Console.ReadLine();
                try
                {
                    Console.WriteLine();
                    switch (input)
                    {
                        case "1":
                            await ShowUI.GetBalance();
                            break;
                        case "2":
                            await ShowUI.GetCapacity();
                            break;
                        case "3":
                            await ShowUI.GetFreePlace();
                            break;
                        case "4":
                            await ShowUI.GetVehicles();
                            break;
                        case "5":
                            await ShowUI.GetVehicle(ShowUI.GetIdFromUser());
                            break;
                        case "6":
                            await ShowUI.PostVehicle(ShowUI.GetIdFromUserRand(), 
                                (int)ShowUI.GetTypeFromUser(), ShowUI.GetBalanceFromUser("balance"));
                            break;
                        case "7":
                            await ShowUI.DeleteVehicle(ShowUI.GetIdFromUser());
                            break;
                        case "8":
                            await ShowUI.GetLastTransactions();
                            break;
                        case "9":
                            await ShowUI.GetAllTransactions();
                            break;
                        case "0":
                            await ShowUI.PutTopUpVehicle(ShowUI.GetIdFromUser(),ShowUI.GetBalanceFromUser("amount"));
                            break;
                        case "exit":
                            return;
                        case "#":
                            ShowUI.CommandMenu();
                            break;
                    }
                    Console.WriteLine();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + "\n");
                }

            }

        }
    }
}
