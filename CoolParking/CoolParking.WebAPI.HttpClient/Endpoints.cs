﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using CoolParking.WebAPI.ModelsJSON;
using Newtonsoft.Json;
using System.Net;


namespace CoolParking.WebAPI.HttpClientUI
{
    static class Endpoints
    {
        private static HttpClient client = new HttpClient();

        public static async Task<string> GetBalance()
        {
            return await GetEndPoint("https://localhost:44380/api/parking/balance");
        }

        public static async Task<string> GetCapacity()
        {
            return await GetEndPoint("https://localhost:44380/api/parking/capacity");
        }

        public static async Task<string> GetFreePlace()
        {
            return await GetEndPoint("https://localhost:44380/api/parking/freePlaces");
        }

        public static async Task<List<VehicleSchema>> GetVehicles()
        {
            var url = "https://localhost:44380/api/vehicles";

            HttpResponseMessage response = await client.GetAsync(url);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<VehicleSchema>>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<VehicleSchema> GetVehicle(string id)
        {
            var url = "https://localhost:44380/api/vehicles/" + id;

            HttpResponseMessage response = await client.GetAsync(url);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<VehicleSchema>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<HttpStatusCode> PostVehicles(VehicleSchema vb)
        {
            var url = "https://localhost:44380/api/vehicles";

            HttpContent req = new StringContent(JsonConvert.SerializeObject(vb), Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync(url, req);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return response.StatusCode;
        }

        public static async Task<HttpStatusCode> DeleteVehicle(string id)
        {
            var url = "https://localhost:44380/api/vehicles/" + id;

            HttpResponseMessage response = await client.DeleteAsync(url);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return response.StatusCode;
        }

        public static async Task<List<TransactionInfoSchema>> GetLastTransactions()
        {
            HttpResponseMessage response = await client.GetAsync("https://localhost:44380/api/transactions/last");

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<TransactionInfoSchema>>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<string> GetAllTransactions()
        {
            HttpResponseMessage response = await client.GetAsync("https://localhost:44380/api/transactions/all");

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        public static async Task<VehicleSchema> PutTopUpVehicle(TopUpVehicleSchema tr)
        {
            HttpContent request = new StringContent(JsonConvert.SerializeObject(tr), Encoding.UTF8, "application/json");

            var response = await client.PutAsync("https://localhost:44380/api/transactions/topUpVehicle", request);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<VehicleSchema>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<string> GetEndPoint(string url)
        {
            HttpResponseMessage response = await client.GetAsync(url);

            await GetResponceMessage(response);

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        public static async Task GetResponceMessage(HttpResponseMessage response)
        {
            if (response.StatusCode == HttpStatusCode.BadRequest || response.StatusCode == HttpStatusCode.NotFound)
                Console.WriteLine(await response.Content.ReadAsStringAsync());
        }
    }

}
