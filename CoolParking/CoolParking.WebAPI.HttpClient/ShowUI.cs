﻿using CoolParking.BL.Models;
using CoolParking.WebAPI.HttpClientUI;
using CoolParking.WebAPI.ModelsJSON;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CoolParking.UI
{

    public static class ShowUI
    {
        public static async Task GetBalance() =>
            Console.WriteLine($"Current balance : {await Endpoints.GetBalance()}");

        public static async Task GetCapacity() =>
            Console.WriteLine($"Capacity : {await Endpoints.GetCapacity()}");

        public static async Task GetFreePlace() =>
            Console.WriteLine($"Free places: {await Endpoints.GetFreePlace()}");



        public static async Task GetVehicles()
        {
            List<VehicleSchema> vehicles = await Endpoints.GetVehicles();
            if (vehicles.Count == 0)
                Console.WriteLine("Parking is free");
            else
                vehicles.ForEach(v => Console.WriteLine(v.ToString()));
        }

        public static async Task GetVehicle(string vehicleId)
        {
            VehicleSchema vehicle = await Endpoints.GetVehicle(vehicleId);
            Console.WriteLine(vehicle.ToString());
        }

        public static async Task PostVehicle(string id, int vehicleType, decimal balance)
        {
            Console.WriteLine(await Endpoints.PostVehicles(new VehicleSchema
            {
                Id = id,
                VehicleType = vehicleType,
                Balance = balance
            }));
        }

        public static async Task DeleteVehicle(string id) =>
            Console.WriteLine(await Endpoints.DeleteVehicle(id));


        public static async Task GetLastTransactions()
        {
            List<TransactionInfoSchema> transactions = await Endpoints.GetLastTransactions();
            if (transactions.Count == 0)
                Console.WriteLine("No current  transactions");
            else
                transactions.ForEach(t => Console.WriteLine(t.ToString()));
        }

        public static async Task GetAllTransactions() =>
            Console.WriteLine(await Endpoints.GetAllTransactions());

        public static async Task PutTopUpVehicle(string id, decimal sum)
        {
            Console.WriteLine(await Endpoints.PutTopUpVehicle(new TopUpVehicleSchema
            {
                Id = id,
                Sum = sum
            }));
        }

        public static string GetIdFromUserRand()
        {
            Console.WriteLine("Generate id randomly? (Y/N)");

            string answer;

            while (true)
            {
                answer = Console.ReadLine();
                Console.WriteLine();
                if (answer == "Y")
                    return Vehicle.GenerateRandomRegistrationPlateNumber();
                else if (answer == "N")
                    return GetIdFromUser();
                else
                    Console.WriteLine("Wrong input!");
            }
        }
        public static string GetIdFromUser()
        {
            bool patternMatch;
            string id;
            do
            {
                Console.WriteLine("Enter Id");
                id = Console.ReadLine();

                patternMatch = Regex.IsMatch(id, Settings.Pattern);
                if (!patternMatch)
                    Console.WriteLine("Wrong Format\n");

            } while (!patternMatch);

            Console.WriteLine();
            return id;
        }

        public static decimal GetBalanceFromUser(string message)
        {
            decimal balance;

            do
            {
                Console.WriteLine($"Enter {message}");
                string BalanceString = Console.ReadLine();
                balance = Convert.ToDecimal(BalanceString);

                if (balance < 1)
                    Console.WriteLine($"{message} can't be less than 1\n");
            } while (balance < 1);
            Console.WriteLine();
            return balance;
        }

        public static VehicleType GetTypeFromUser()
        {
            string type;

            Console.WriteLine("Choose vehicle type");
            PrintVehicleTypes();

            do
            {
                Console.WriteLine("\nEnter type num");
                type = Console.ReadLine();
                if (type != "0" && type != "1" && type != "2" && type != "3") Console.WriteLine("Wrong num");
            } while (type != "0" && type != "1" && type != "2" && type != "3");
            Console.WriteLine();
            switch (type)
            {
                case "0":
                    return VehicleType.PassengerCar;
                case "1":
                    return VehicleType.Truck;
                case "2":
                    return VehicleType.Bus;
                case "3":
                    return VehicleType.Motorcycle;
                default:
                    return VehicleType.PassengerCar;
            }
        }
        private static void PrintVehicleTypes()
        {
            Console.WriteLine($"0 - {VehicleType.PassengerCar}");
            Console.WriteLine($"1 - {VehicleType.Truck}");
            Console.WriteLine($"2 - {VehicleType.Bus}");
            Console.WriteLine($"3 - {VehicleType.Motorcycle}");
        }

        public static void CommandMenu()
        {
            Console.WriteLine("1 - Display the current balance of the Parking.");
            Console.WriteLine("2 - Display the capacity of the Parking");
            Console.WriteLine("3 - Display the number of free parking spaces (free X with Y).");
            Console.WriteLine("4 - Display all vehicles");
            Console.WriteLine("5 - Display vehicle by id");
            Console.WriteLine("6 - Post vehicle");
            Console.WriteLine("7 - Delete vehicle by id");
            Console.WriteLine("8 - Get Last Transactions");
            Console.WriteLine("9 - Get All Transactions");
            Console.WriteLine("0 - Top Up Vehicle");
            Console.WriteLine("exit - to exit the program");
        }
    }
}
