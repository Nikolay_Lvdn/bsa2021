﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        // Amount of money for the parking
        public decimal Sum { get; private set; }
        // Transaction time
        public DateTime TransactionTime { get; private set; }
        // Vehicle Plate Number
        public string VehicleId { get; private set; }

        public TransactionInfo(decimal sum, DateTime transactionTime, string vehicleId)
        {
            Sum = sum;
            TransactionTime = transactionTime;
            VehicleId = vehicleId;
        }

        public override string ToString()
        {
            return $" TransactionTime: {TransactionTime} | VehicleId: {VehicleId} | Sum: {Sum}";
        }
    }

}