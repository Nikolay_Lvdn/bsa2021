﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        // Initial balance of Parking
        public static decimal ParkingInitialBalance { get; }
        // Parking capacity
        public static int ParkingCapacity { get; }
        // Tariffs depending on the vehicle
        public static Dictionary<VehicleType, decimal> Tariffs { get; }
        // The period of payment write-off
        public static int PaymentIntervalInSec { get; }
        // The period of writing to the log
        public static int LogSaveIntervalInSec { get; }
        // Fine ratio
        public static decimal FineRatio { get; }
        // Default path to log file
        public static string LogFilePath { get; }
        // Pattern for the plate number
        public static string Pattern { get; }
        static Settings()
        {
            ParkingInitialBalance = 0.0M;
            ParkingCapacity = 10;
            PaymentIntervalInSec = 5;
            LogSaveIntervalInSec = 60;
            FineRatio = 2.5M;
            LogFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
            Pattern = @"[A-Z]{2}-[0-9]{4}-[A-Z]{2}";
            Tariffs = new Dictionary<VehicleType, decimal>()
            {
                // PassengerCar tarrif
                {VehicleType.PassengerCar, 2.0M},
                // Truck tarrif
                {VehicleType.Truck, 5.0M},
                // Bus tarrif
                {VehicleType.Bus, 3.5M},
                // Motorcycle tarrif
                {VehicleType.Motorcycle, 1.0M}
            };
        }
    }
}