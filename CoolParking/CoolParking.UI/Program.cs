﻿using System;
using System.Linq;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System.Text.Json;

namespace CoolParking.UI
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Greetings and salutations!\n" +
                "This is my bsa2021 task 2\n" +
                "You are welcome :)\n");

            ParkingService parkingService = new ParkingService();

            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Enter command(# - to call /help):");
                Console.ForegroundColor = ConsoleColor.White;
                string input = Console.ReadLine();
                try
                {
                    Console.WriteLine();
                    switch (input)
                    {
                        case "1":
                            ShowUI.CurrentBalance(parkingService.GetBalance().ToString());
                            break;
                        case "2":
                            ShowUI.MoneyGainedForLastParkingTransactions(parkingService.GetLastParkingTransactions());
                            break;
                        case "3":
                            ShowUI.FreePlaces(parkingService.GetFreePlaces().ToString(), parkingService.GetCapacity().ToString());
                            break;
                        case "4":
                            ShowUI.LastParkingTransactions(parkingService.GetLastParkingTransactions());
                            break;
                        case "5":
                            ShowUI.LogFile(parkingService.ReadFromLog());
                            break;
                        case "6":
                            ShowUI.Vehicles(parkingService.GetVehicles());
                            break;
                        case "7":
                            Console.WriteLine("Enter all information to add vehicle");
                            parkingService.AddVehicle(new Vehicle(ShowUI.GetIdFromUser(),
                                           ShowUI.GetTypeFromUser(), ShowUI.GetBalanceFromUser("balance")));
                            break;
                        case "8":
                            Console.WriteLine("Enter id to delete vehicle");
                            parkingService.RemoveVehicle(ShowUI.GetIdFromUser());
                            break;
                        case "9":
                            Console.WriteLine("Enter id and sum to topup the vehicle");
                            parkingService.TopUpVehicle(ShowUI.GetIdFromUser(), ShowUI.GetBalanceFromUser("TopUp"));
                            break;
                        case "exit":
                            parkingService.Dispose();
                            return;
                        case "#":
                            ShowUI.CommandMenu();
                            break;
                    }
                    Console.WriteLine();
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message+"\n");
                }
               
            }


        }
    }
}
