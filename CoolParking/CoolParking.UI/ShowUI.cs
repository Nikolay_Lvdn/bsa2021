﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.Json;
using System.Text.RegularExpressions;
using CoolParking.BL.Models;

namespace CoolParking.UI
{
    
    public static class ShowUI
    {
        public static void CurrentBalance(string balance) => Console.WriteLine($"Current balance is : {balance}");
        public static void MoneyGainedForLastParkingTransactions(TransactionInfo[] transactions) => 
            Console.WriteLine($"The amount of earned expenses for the current period is : {transactions.Sum(t => t.Sum)}");
        public static void FreePlaces(string freePlaces,string capacity) => 
            Console.WriteLine($"Free {freePlaces} from {capacity}");

        public static void LastParkingTransactions(TransactionInfo[] transactions)
        {
            if (transactions.Count() == 0)
                Console.WriteLine("There have been no recent transactions");
            else
                foreach (var t in transactions)
                    t.ToString();
        }

        public static void LogFile(string LogText)
        {
            if (String.IsNullOrEmpty(LogText))
                Console.WriteLine("Nothing to read");
            else
                Console.WriteLine($"Log history : \n{LogText}");
        }

        public static void Vehicles(ReadOnlyCollection<Vehicle> vehicles)
        {
            if (vehicles.Count() == 0)
                Console.WriteLine("There are no vehicles");
            else
            {
                Console.WriteLine("All vehicles:");
                foreach (var v in vehicles)
                {
                    Console.WriteLine(v.ToString()); 
                }
            }
        }

        public static string GetIdFromUser()
        {
            bool patternMatch;
            string id;
            do
            {
                Console.WriteLine("Enter Id (enter 0 to generate randomly)");
                id = Console.ReadLine();

                if (id == "0")
                    return Vehicle.GenerateRandomRegistrationPlateNumber();

                patternMatch = Regex.IsMatch(id, Settings.Pattern);

                if (!patternMatch) Console.WriteLine("Wrong Format\n");

            } while (!patternMatch);

            Console.WriteLine();
            return id;
        }

        public static decimal GetBalanceFromUser(string message)
        {
            decimal balance;

            do
            {
                Console.WriteLine($"Enter {message}");
                string BalanceString = Console.ReadLine();
                balance = Convert.ToDecimal(BalanceString);
                if (balance < 1) Console.WriteLine("You can't left no money\n");
            } while (balance < 1);
            Console.WriteLine();
            return balance;
        }

        public static VehicleType GetTypeFromUser()
        {
            string type;

            Console.WriteLine("Choose vehicle type");
            PrintVehicleTypes();

            do
            {
                Console.WriteLine("\nEnter type num");
                type = Console.ReadLine();
                if (type != "0" && type != "1" && type != "2" && type != "3") Console.WriteLine("Wrong num");
            } while (type != "0" && type != "1" && type != "2" && type != "3");
            Console.WriteLine();
            switch (type)
            {
                case "0":
                    return VehicleType.PassengerCar;
                case "1":
                    return VehicleType.Truck;
                case "2":
                    return VehicleType.Bus;
                case "3":
                    return VehicleType.Motorcycle;
                default:
                    return VehicleType.PassengerCar;
            }
        }
        private static void PrintVehicleTypes()
        {
            Console.WriteLine($"0 - {VehicleType.PassengerCar}");
            Console.WriteLine($"1 - {VehicleType.Truck}");
            Console.WriteLine($"2 - {VehicleType.Bus}");
            Console.WriteLine($"3 - {VehicleType.Motorcycle}");
        }

        public static void CommandMenu()
        {
            Console.WriteLine("1 - Display the current balance of the Parking.");
            Console.WriteLine("2 - Display the amount of money earned for the current period (before logging)");
            Console.WriteLine("3 - Display the number of free parking spaces (free X with Y).");
            Console.WriteLine("4 - Display all Parking Transactions for the current period (before logging).");
            Console.WriteLine("5 - Display the transaction history (reading data from the Transactions.log file).");
            Console.WriteLine("6 - Display a list of vehicles in the Parking.");
            Console.WriteLine("7 - Put the vehicle in the parking lot.");
            Console.WriteLine("8 - Pick up the Vehicle from the Parking.");
            Console.WriteLine("9 - Replenish the balance of a specific Tr. means.");
            Console.WriteLine("exit - to exit the program");
        }
    }
}
