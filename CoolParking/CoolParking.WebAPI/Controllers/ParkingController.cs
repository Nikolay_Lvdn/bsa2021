﻿using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService _parkingService;

        public ParkingController(IParkingService service)
        {
            _parkingService = service;
        }

        // GET: api/<ParkingController>/capacity
        [HttpGet]
        [Route("capacity")]
        public ActionResult<int> GetCapacity() => Ok(_parkingService.GetCapacity());


        // GET api/<ParkingController>/balance
        [HttpGet]
        [Route("balance")]
        public ActionResult<decimal> GetBalance() => Ok(_parkingService.GetBalance());

        // GET api/<ParkingController>/freePlaces
        [HttpGet]
        [Route("freePlaces")]
        public ActionResult<int> GetFreePlaces() => Ok(_parkingService.GetFreePlaces());

    }
}
