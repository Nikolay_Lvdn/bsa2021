﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using CoolParking.WebAPI.ModelsJSON;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private IParkingService _parkingService;
        private string _pattern;

        public VehiclesController(IParkingService service)
        {
            _parkingService = service;
            _pattern = Settings.Pattern;
        }

        // GET: api/<VehiclesController>
        [HttpGet]
        public ActionResult<List<VehicleSchema>> Get() => Ok(_parkingService.GetVehicles());



        // GET api/<VehiclesController>/vehicleId
        [HttpGet("{id}")]
        public ActionResult<VehicleSchema> Get(string id)
        {
            if (!Regex.IsMatch(id, _pattern))
                return BadRequest("Wrong Id Format");

            var vehicles = _parkingService.GetVehicles();
            var vehicle = vehicles.FirstOrDefault(v => v.Id == id);

            if (vehicle == null)
                return NotFound();
            else
            {
                return Ok(new VehicleSchema
                {
                    Id = vehicle.Id,
                    VehicleType = (int)vehicle.VehicleType,
                    Balance = vehicle.Balance
                });
            }

        }

        // POST api/<VehiclesController>
        [HttpPost]
        public ActionResult<VehicleSchema> Post([FromBody] VehicleSchema vehicleJson)
        {
            if(!Enum.IsDefined(typeof(VehicleType),vehicleJson.VehicleType))
                return BadRequest();

            if (vehicleJson == null)
                return BadRequest();

            try
            {
                _parkingService.AddVehicle(new Vehicle(vehicleJson.Id,
                    (VehicleType)vehicleJson.VehicleType, vehicleJson.Balance));

                return CreatedAtAction("POST", vehicleJson);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE api/<VehiclesController>/vehicleId
        [HttpDelete("{id}")]
        public ActionResult Delete([FromRoute] string id)
        {
            if (!Regex.IsMatch(id, _pattern))
                return BadRequest();

            var vehicles = _parkingService.GetVehicles();
            var vehicle = vehicles.FirstOrDefault(v => v.Id == id);
            if (vehicle == null)
                return NotFound();

            try
            {
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }

            
        }
    }
}
