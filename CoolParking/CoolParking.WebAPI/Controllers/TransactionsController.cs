﻿using CoolParking.BL.Interfaces;
using CoolParking.WebAPI.ModelsJSON;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private IParkingService _parkingService;

        public TransactionsController(IParkingService service)
        {
            _parkingService = service;
        }

        // GET: api/<TransactionsController>/capacity
        [HttpGet]
        [Route("last")]
        public ActionResult<List<TransactionInfoSchema>> GetLastParkingTransactions() => 
            Ok(_parkingService.GetLastParkingTransactions());

        // GET: api/<TransactionsController>/all
        [HttpGet("all")]
        public ActionResult<string> GetAllTransaction()
        {
            try
            {
                var logtext = _parkingService.ReadFromLog();
                return Ok(logtext);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // PUT: api/<TransactionsController>/topUpVehicle
        [HttpPut("topUpVehicle")]
        public ActionResult<VehicleSchema> PutTopUpVehicle([FromBody] TopUpVehicleSchema topUpVehicleSchema)
        {
            if (topUpVehicleSchema == null)
                return BadRequest();

            try
            {
                _parkingService.TopUpVehicle(topUpVehicleSchema.Id, topUpVehicleSchema.Sum);
                return Ok(_parkingService.GetVehicles().FirstOrDefault(v => v.Id == topUpVehicleSchema.Id));
            }
            catch (ArgumentNullException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
