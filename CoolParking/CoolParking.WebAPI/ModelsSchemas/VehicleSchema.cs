﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.ModelsJSON
{
    public class VehicleSchema
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("vehicleType")]
        public int VehicleType { get; set; }

        [JsonProperty("balance")]
        public decimal Balance { get; set; }

        public override string ToString()
        {
            return $" Id: {Id} | VehicleType: {VehicleType} | Balance: {Balance}";
        }

    }
}
